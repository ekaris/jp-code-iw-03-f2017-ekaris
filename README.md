# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository holds the code for the junior independent work of Evelyn Karis, including three programs
that are tools for data analysis involving two newspaper publications.

### How do I get set up? ###

Download all of the files and make sure that you have the following Python packages installed on your
computer:
SortedDict
Counter
ngrams
matplotlib.pylab

### Command-line Instructions ###
ngram.py: Plots the relative frequency of the query term or phrase in both The Daily Princetonian and Town Topics
newspaper publications from 1946 to 2015. 

python ngram.py "query"

ngramsearch.py: This program takes 2 arguments, a search query and a year, in that order and returns the raw
frequency of the query in both The Daily Princetonian and Town Topics for that year.

python ngramsearch.py "query" year

mostcommon.py: This program takes 2 integer arguments, n and m, where n specifies the length
of the n-gram and m specficies the desired number of most common words, and creates files
for each year from 1946 to 2015 for both The Daily Princetonian and Town Topics with the
list of the most common m n-grams.

python mostcommon.py n m

### Note ###
Queries that result in the creation of n-grams where n > 3 will take a significantly longer time to run than
queries where n <=3. 

### Who do I talk to? ###

Evelyn Karis
Princeton University
Class of 2019